package base;
import ij.process.*;

/**
 * Represents any edge detection process that determines angles.
 * 
 * @author Tim, James
 */
public abstract class EdgeDetection extends ImageProcess
{

	/**
	 * Creates an EdgeDetection associated with the image represented by the given ImageProcessor.
	 * 
	 * @param ip the ImageProcessor associated with this EdgeDetection
	 */
	protected EdgeDetection(ImageProcessor ip)
	{
		super(ip);
	}

	/**
	 * Find edges and edge angles in the ImageProcessor
	 */
	abstract public void run();

	// TODO: clarify in this javadoc comment whether the angles returned are only along the edge
	//       or if they are blurred (never mind whether or not blurring was used to find them).
	/**
	 * Gets a FloatProcessor containing the angles calculated by this
	 * EdgeDetection.
	 * 
	 * @return a FloatProcessor containing the angles calculated by this
	 * EdgeDetection.
	 * @throws IllegalStateException thrown if this method is called before run()
	 */
	abstract FloatProcessor getAngles() throws IllegalStateException;
	/**
	 * Gets a FloatProcessor containing the magnitude calculated by this
	 * EdgeDetection.
	 * 
	 * @return a FloatProcessor containing the magnitude calculated by this
	 * EdgeDetection.
	 * @throws IllegalStateException thrown if this method is called before run()
	 */
	abstract FloatProcessor getMagnitude() throws IllegalStateException;
}
