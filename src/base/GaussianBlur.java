package base;
import ij.process.*;

// TODO: specify in the following javadoc comment whether or not ip is affected by the blur
/**
 * Represents a gaussian blurring process that blurs angles
 * 
 * @author James
 * 
 * @deprecated
 */
public class GaussianBlur extends Blur
{
	/**
	 * Creates a GaussianBlur process associated with the image represented by
	 * the given FloatProcessor, which specifies angles, and the given
	 * ImageProcessor, which specifies magnitudes. The blur uses the specified
	 * sigma value and window size
	 * 
	 * @param ip the ImageProcessor associated with this Blur
	 *           and specifying magnitudes
	 * @param angles the FloatProcessor associated with this Blur
	 *               and specifying angles in radians
	 * @param sigma the radius of the gaussian blur
	 * @param windowSize the size of the convolution window
	 */
	public GaussianBlur(ImageProcessor ip, FloatProcessor angles, float sigma, float windowSize)
	{
		super(ip, angles);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Blurs the angle image.
	 */
	@Override
	public void run()
	{
		// TODO Auto-generated method stub

	}

}
