package base;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.awt.Color;
import ij.process.*;

/**
 * Represents any process performed on an image.
 * 
 * @author Tim, James
 */
public abstract class ImageProcess
{
	protected ImageProcessor ip;
	
	/**
	 * Creates an ImageProcess associated with the image represented by the
	 * given ImageProcessor.
	 * 
	 * NOTE: It may be useful to modify this constructor to make a copy of the
	 * ImageProcessor, so that run() doesn't modify the original. Maybe.
	 * 
	 * @param ip the ImageProcessor associated with this ImageProcess
	 */
	protected ImageProcess(ImageProcessor ip)
	{
		this.ip = ip;
	}
	
	/**
	 * Gets the ImageProcessor associated with this ImageProcess.
	 * The return value of this method is affected by run().
	 * 
	 * @return the ImageProcessor associated with this ImageProcess
	 */
	public ImageProcessor getImageProcessor()
	{
		return this.ip;
	}
}
