package base;

/**
 * Represents a 2D point.
 * 
 * @author Tim
 *
 * @param <T> the type of the coordinates. Point&lt;Integer&gt; is a good choice
 */
public class Point<T>
{
	private T x;
	private T y;
	
	/**
	 * Creates the point with the given coordinates
	 * 
	 * @param x the x coordinate
	 * @param y the y coordinate
	 */
	public Point(T x, T y)
	{
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets the x coordinate
	 * 
	 * @return the x coordinate
	 */
	public T getX()
	{
		return this.x;
	}
	
	/**
	 * Gets the y coordinate
	 * 
	 * @return the y coordinate
	 */
	public T getY()
	{
		return this.y;
	}
}
