package base;
import java.awt.Color;

import ij.process.FloatProcessor;
import ij.process.ImageProcessor;


/**
 * Represents the process of converting a color image into a black and white
 * image by looking for colors close to a given color.
 * 
 * @author James
 */
public class ColorExtraction extends ImageProcess
{
	private ApproximateColor color;
	private FloatProcessor result = null;
	/**
	 * Creates an ColorExtraction associated with the image represented by the
	 * given ImageProcessor.
	 * 
	 * @param ip the ImageProcessor associated with this ImageProcess
	 */
	public ColorExtraction(ImageProcessor ip, ApproximateColor color)
	{
		super(ip);
		this.color = color;
	}
	
	// TODO: Note in comment whether colors matching the ApproximateColor will become black or white.
	/**
	 * Converts the ImageProcessor to a black and white image
	 */
	public void run()
	{
		int width = ip.getWidth();
		int height = ip.getHeight();
		result = new FloatProcessor(width, height);
		//loop through all points
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				if (color.matches(new Color(ip.get(x,y)))) {
					// if matches, set 0
					result.setf(x,y,0f);
				} else {
					// else set 1
					result.setf(x,y,Float.MAX_VALUE);
				}
			}
		}
	}
	
	/**
	 * Gets a FloatProcessor containing the BW result
	 * 
	 * @return a FloatProcessor containing the angles calculated by this
	 * EdgeDetection.
	 * @throws IllegalStateException thrown if this method is called before run()
	 */
	public FloatProcessor getBW() throws IllegalStateException {
		if (result == null) {
			throw new IllegalStateException("Called getBW before calling run");
		}
		return result;
	}
	

}
