package base;
import ij.process.*;

/**
 * Represents a process that blurs angles, magnitudes, or both.
 * 
 * @author Tim
 */
public abstract class Blur extends VectorImageProcess
{
	/**
	 * Creates a Blur process associated with the image represented by
	 * the given FloatProcessor, which specifies angles, and the given
	 * ImageProcessor, which specifies magnitudes.
	 * 
	 * @param ip the ImageProcessor associated with this Blur
	 *           and specifying magnitudes
	 * @param angles the FloatProcessor associated with this Blur
	 *               and specifying angles in radians
	 */
	protected Blur(ImageProcessor ip, FloatProcessor angles)
	{
		super(ip, angles);
	}

}
