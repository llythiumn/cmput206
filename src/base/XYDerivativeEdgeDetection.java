package base;
import ij.ImagePlus;
import ij.plugin.filter.GaussianBlur;
import ij.process.*;
import filters.*;
import sources.*;

/**
 * Represents an edge detection using simple x and y derivative convolution windows
 * 
 * @author James
 */
public class XYDerivativeEdgeDetection extends EdgeDetection
{

	private FloatProcessor angles = null;
	private FloatProcessor magnitude = null;
	/**
	 * Creates an XYDerivativeEdgeDetection associated with the image
	 * represented by the given ImageProcessor.
	 * 
	 * @param ip the ImageProcessor associated with this XYDerivativeEdgeDetection
	 */
	public XYDerivativeEdgeDetection(ImageProcessor ip)
	{
		super(ip);
	}

	/* (non-Javadoc)
	 * @see EdgeDetection#run()
	 */
	@Override
	public void run()
	{
		// Set up all proceedures
		FloatProcessor X, Y;
		XFilter Xfilt = new XFilter();
		YFilter Yfilt = new YFilter();
		MagnitudeFilter Magnitudefilt = new MagnitudeFilter();
		Accurate_Gaussian_Blur Blurfilt = new Accurate_Gaussian_Blur();
		AngleFilter Anglefilt = new AngleFilter(Math.PI / 2);
		// execute the filter
		X = Xfilt.run(ip);
		Y = Yfilt.run(ip);
		// calculate the magnitude image
		magnitude = Magnitudefilt.run(X, Y);
		// secondary filter to blur the images
		Blurfilt.blurFloat(X, 0.5, 0.5, 0.01);
		Blurfilt.blurFloat(Y, 0.5, 0.5, 0.01);
		// perform angle calculations to get the blurred angles
		angles = Anglefilt.run(X, Y);
	}

	/* (non-Javadoc)
	 * @see EdgeDetection#getAngles()
	 */
	@Override
	public FloatProcessor getAngles() throws IllegalStateException
	{
		if (angles == null) {
			throw new IllegalStateException("Have not ran the XYDerivativeEdgeDetection, angles is still null");
		}
		return angles;
	}


	/* (non-Javadoc)
	 * @see EdgeDetection#getMagnitude()
	 */
	@Override
	public FloatProcessor getMagnitude() throws IllegalStateException
	{
		if (magnitude == null) {
			throw new IllegalStateException("Have not ran the XYDerivativeEdgeDetection, magnitude is still null");
		}
		return magnitude;
	}

}

