package base;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import ij.process.*;

import java.util.*;

/**
 * Represents the process of creating all of the SVG elements associated with
 * one fill color.
 * 
 * @author Ian, Mike
 */
public class BinarySvgCreation extends VectorImageProcess
{

	/**
	 * Creates a BinarySvgCreation associated with the image represented by
	 * the given FloatProcessor, which specifies rotated edge angles,
	 * the given ImageProcessor, which specifies edge magnitudes, the given
	 * color, and the given points of interest.
	 * 
	 * @param ip the ImageProcessor associated with this BinarySvgCreation
	 *           and specifying crisp edge magnitudes
	 * @param angles the FloatProcessor associated with this VectorImageProcess
	 *               and specifying rotated edge angles in radians
	 * @param color the color to fill all shapes with
	 * @param pois the points of interest to use as endpoints for bezier curves
	 */
	
	private Color color;
	private ArrayList<Point<Integer>> pois;
	private ArrayList<GeneralPath> Paths = null;
	
	public BinarySvgCreation(ImageProcessor ip, FloatProcessor angles, Color color, ArrayList<Point<Integer>> pois)
	{
		super(ip, angles);
		this.color = color;
		this.pois  = pois;
		// TODO Auto-generated constructor stub
		
		/* *****************************************
		 		ArrayList<GeneralPath> Paths = new ArrayList<GeneralPath>(); 
				
		boolean[] checked = new boolean[pois.length];
		
		//* Loop through the points of interest
		for(int i=0; i < pois.length; i++)
		{
			
			//ADD THE CHECK INTO POINT?
			//* --Check if it has been checked off
			if(checked[i])
			{
				continue;
			}
			
			//Procedure(ip, angles, color, pois, checked);
			GeneralPath newPath;
		    newPath = Procedure(ip, angles, color, pois, checked);
			Paths.add(newPath);
			  	  
		}
		
		return Paths;  // Does the class type need to change?		
		********************************************************
		*/
		 
		
	}
	
	public GeneralPath Procedure(ImageProcessor ip, FloatProcessor angles, Color color, ArrayList<Point<Integer>> pois, boolean[] checked)
	{
		GeneralPath currentPath = new GeneralPath();
		//Get first point of interest pois[0]
		int initialX = pois.get(0).getX();
		int initialY = pois.get(0).getY();
		//float initialAngle = angles.getPixelValue(initialX, initialY);
		
		//Make initial point in Path
		currentPath.moveTo(initialX, initialY);
		checked[0] = true;
		
		
		boolean following = true;
	
		int currentX = initialX;
		int currentY = initialY;
		
		int previousX = 0;
		int previousY = 0;
		
		int referenceX = currentX;
		int referenceY = currentY; 
		
		int straight = 0;
		
		float lastAngle;
		float currentAngle;
		float sweep = 0;
		
		//Follow angles to next point of interest
		while(following)
		{	
					
			lastAngle = angles.getPixelValue(currentX, currentY);
			int[] next = nextPoint(currentX, currentY, ip, angles, previousX, previousY);
			currentX = next[0];// nextPoint.X
		    currentY = next[1];// nextPoint.Y
		    
		    //Check if there is a nearby poi
		    
		    
		    //**************
		    //Check if Current Point is Initial Point
		    if(currentX == initialX && currentY == initialY)
		    {
		    	currentPath.closePath();
		    	following = false;
		    	break;
		    }
		    //**************
		    
		    //--Stuff for calculating Sweep
			currentAngle = angles.getPixelValue(currentX, currentY);
			float angleDifference = lastAngle - currentAngle;
		    sweep = sweep + angleDifference;
		    
		    //STRAIGHT LINE
		    //--check if straight line (threshold)
		    if (angleDifference < Math.PI/32)
		    {
		    	straight = straight + 1;
			} 
		    else if(angleDifference >= Math.PI/32  && straight > 5)
		    {
		    //	Create current point with a line and continue
		    //  end point --> Lineto the general path
		        currentPath.lineTo(currentX, currentY);
		        straight = 0;
		        
				referenceX = currentX;
				referenceY = currentY;
		        
		    //  This point does not need to be checked off.
		    }
		    
		    //CURVES
		    //Get angle of new point (and reverse it)
		    
		    //Calculate intersect point
		    //Quadto the new point and control point
			//Check off new point
		    
		    if(isPointOfInterest(currentX, currentY, pois))
		    {
		    	float newAngle = angles.getPixelValue(currentX, currentY);
		    	
				float[] intersect = calculateIntersect(referenceX, referenceY, lastAngle, currentX, currentY, newAngle);
				float intersectX = intersect[0];
			    float intersectY = intersect[1];
		    	
		    	currentPath.quadTo(currentX, currentY, intersectX, intersectY);
		    	//currentPath.lineTo(currentX, currentY);
		    	int i = pointIndex(currentX, currentY, pois);
		    	checked[i] = true;
		    }
		    
		}
		
		//Still need some way to cut
		if(sweep > Math.PI) //GREATER OR LESS THAN?
		{
			//CounterClockwise
			//color = CUT;
		} 
		
		//Add a close path command? Or will Lineto close it automatically?
		return currentPath;
	}
	
	
	public int[] nextPoint(int currentX, int currentY, ImageProcessor ip, FloatProcessor angles, int previousX, int previousY)
	{
		int[] XY = {0,0};
		int nextX = 0;
		int nextY = 0;

		int[] neighbors = new int[10]; 
		
		//Add exception for pixels off screen?
		neighbors[0] = ip.get(currentX - 1, currentY - 1);
		neighbors[1] = ip.get(currentX    , currentY - 1);
		neighbors[2] = ip.get(currentX + 1, currentY - 1);
		neighbors[3] = ip.get(currentX - 1, currentY    );
		//neighbors[4] = ip.get(currentX    , currentY);
		neighbors[5] = ip.get(currentX + 1, currentY    );
		neighbors[6] = ip.get(currentX - 1, currentY + 1);
		neighbors[7] = ip.get(currentX    , currentY + 1);
		neighbors[8] = ip.get(currentX + 1, currentY + 1);
		
		
		//Special Case for the initial Point
		if(previousX == 0 && previousY == 0)
		{
			//int specialMagnitude = ip.get(currentX, currentY); 
			float specialAngle = angles.getPixelValue(currentX, currentY);
			
			if((specialAngle > 0 && specialAngle < Math.PI/2) || specialAngle == 2*Math.PI )
			{
				//get which points
				previousX = currentX - 1;
				previousY = currentY - 1;
				if(neighbors[3] > neighbors[0])
				{
					previousX = currentX - 1;
					previousY = currentY;
				}
				if(neighbors[6] > neighbors[3])
				{
					previousX = currentX - 1;
					previousY = currentY - 1;
				}
				if(neighbors[7] > neighbors[6])
				{
					previousX = currentX;
					previousY = currentY + 1;
				}
				if(neighbors[8] > neighbors[7])
				{
					previousX = currentX + 1;
					previousY = currentY + 1;
				}

			}
			else if((specialAngle >= Math.PI/2) && (specialAngle < Math.PI))
			{
				//Look at ones in front
				//get which points
				previousX = currentX - 1;
				previousY = currentY + 1;
				if(neighbors[7] > neighbors[6])
				{
					previousX = currentX;
					previousY = currentY + 1;
				}
				if(neighbors[8] > neighbors[7])
				{
					previousX = currentX + 1;
					previousY = currentY + 1;
				}
				if(neighbors[5] > neighbors[8])
				{
					previousX = currentX + 1;
					previousY = currentY;
				}
				if(neighbors[2] > neighbors[5])
				{
					previousX = currentX + 1;
					previousY = currentY - 1;
				}

			}
			else if(specialAngle >= Math.PI && specialAngle < 3*(Math.PI/2))
			{
				//Look at ones in front
				//get which points
				previousX = currentX + 1;
				previousY = currentY + 1;
				if(neighbors[5] > neighbors[8])
				{
					previousX = currentX + 1;
					previousY = currentY;
				}
				if(neighbors[2] > neighbors[5])
				{
					previousX = currentX + 1;
					previousY = currentY - 1;
				}
				if(neighbors[1] > neighbors[2])
				{
					previousX = currentX;
					previousY = currentY - 1;
				}
				if(neighbors[0] > neighbors[1])
				{
					previousX = currentX - 1;
					previousY = currentY - 1;
				}
				
			}
			else if(specialAngle >= 3*(Math.PI)/2 && specialAngle < 2*(Math.PI))
			{
				//Look at ones in front
				//get which points
				previousX = currentX + 1;
				previousY = currentY - 1;
				if(neighbors[1] > neighbors[2])
				{
					previousX = currentX;
					previousY = currentY - 1;
				}
				if(neighbors[0] > neighbors[1])
				{
					previousX = currentX - 1;
					previousY = currentY - 1;
				}
				if(neighbors[3] > neighbors[0])
				{
					previousX = currentX - 1;
					previousY = currentY;
				}
				if(neighbors[6] > neighbors[3])
				{
					previousX = currentX - 1;
					previousY = currentY + 1;
				}
				
			}

			
		}
		
		//Calculate the next pixel to follow
		//look at magnitudes and angles
		//int magnitude = ip.get(currentX, currentY); 
		//float angle = angles.getPixelValue(currentX, currentY);

		//nextX = currentX + 1;
		//nextY = currentY - 1;
		if((previousX != currentY-1) && (previousY != currentY-1))
		{
			nextX = currentX - 1;
			nextY = currentY - 1;
		}
		if((neighbors[1] > neighbors[0]) && ((previousX != currentX) || (previousY != currentY-1)))
		{
			nextX = currentX ;
			nextY = currentY - 1;
		}
		if((neighbors[2] > neighbors[1]) && ((previousX != currentX + 1) || (previousY != currentY-1)))
		{
			nextX = currentX + 1;
			nextY = currentY - 1;
		}
		if((neighbors[3] > neighbors[2]) && ((previousX != currentX - 1) || (previousY != currentY)))
		{
			nextX = currentX - 1;
			nextY = currentY;
		}
		if((neighbors[5] > neighbors[3]) && ((previousX != currentX + 1) || (previousY != currentY)))
		{
			nextX = currentX + 1;
			nextY = currentY;
		}
		if((neighbors[6] > neighbors[5]) && ((previousX != currentX - 1) || (previousY != currentY + 1)))
		{
			nextX = currentX - 1;
			nextY = currentY + 1;
		}
		if((neighbors[7] > neighbors[6]) && ((previousX != currentX) || (previousY != currentY + 1)))
		{
			nextX = currentX;
			nextY = currentY + 1;
		}
		if((neighbors[8] > neighbors[7]) && ((previousX != currentX + 1) || (previousY != currentY + 1)))
		{
			nextX = currentX + 1;
			nextY = currentY + 1;
		}
		
		XY[0] = nextX;
		XY[1] = nextY;
		return XY;
		
	}
	
	public boolean isPointOfInterest(int X, int Y, ArrayList<Point<Integer>> pois)
	{
		boolean poi = false;
		
		for(int i = 0; i < pois.size(); i++)
		{
			//int initialX = pois.get(0).getX();
			int poiX = pois.get(i).getX();
			int poiY = pois.get(i).getY();
			
			if(X == poiX && Y ==poiY)
			{
				poi = true;
				break;
			}
		}
		
		return poi;
	}

	public int pointIndex(int X, int Y, ArrayList<Point<Integer>> pois)
	{
		int index = 0;
		
		for(int i = 0; i < pois.size(); i++)
		{
			//int poiX = pois.get(i).getX();
			int poiX = pois.get(i).getX();
			int poiY = pois.get(i).getY();
			
			if(X == poiX && Y == poiY)
			{
				index = i;
				break;
				
			}
		}
	
		return index;
	}
	
	public float[] calculateIntersect(int referenceX, int referenceY, float lastAngle, int currentX, int currentY, float newAngle)
	{
		float[] intersectXY = {0,0};
		
		
		float slope1 = (float)Math.tan(lastAngle);
		float slope2 = (float)Math.tan(newAngle);
		float intercept1 = referenceY - slope1 * referenceX;
		float intercept2 = currentY - slope2 * currentX;
		float xIntersection = (intercept2-intercept1)/(slope1-slope2);
		float yIntersection = slope1 * xIntersection + intercept1;
		
		intersectXY[0] = xIntersection;
		intersectXY[1] = yIntersection;
		return intersectXY;
		
	}	
	
	//Comparison between the Floats for points of interest	
	
	/**
	 * Creates all of the SVG elements associated with this BinarySvgCreation
	 */
	@Override
	public void run()
	{
		//public ArrayList<GeneralPath> run(ImageProcessor ip, FloatProcessor angles, Color color, Point<Integer>[] pois)
		//ImageProcessor ip, FloatProcessor angles, Color color, Point<Integer>[] pois
		
		Paths = new ArrayList<GeneralPath>(); 
				
		boolean[] checked = new boolean[pois.size()];
		
		//* Loop through the points of interest
		for(int i=0; i < pois.size(); i++)
		{
			
			//ADD THE CHECK INTO POINT?
			//* --Check if it has been checked off
			if(checked[i])
			{
				continue;
			}
			
			//Procedure(ip, angles, color, pois, checked);
			GeneralPath newPath;
		    newPath = Procedure(ip, angles, color, pois, checked);
			Paths.add(newPath);
			
			  	  
		}
		
		//public getPaths()
		//{
		//	return this.Paths;
		//}
		
		// TODO Auto-generated method stub

		// For every two connected points of interest, connect them with a
		// quadratic bezier curve. Note that a quadratic bezier curve is
		// completely defined by its endpoints and the angles at those endpoints.
		//
		// Join bezier curves using an ExtendedGeneralPath.
		//
		// Try to figure out how to color in the ExtendedGeneralPath.
		// If you can't figure it out, just find the paths, and let James
		// know that you need another step in the process to fill in the shapes.
	}
	
	public ArrayList<GeneralPath> getPoints() throws IllegalStateException
	{
		// TODO Auto-generated method stub
		if (Paths == null) {
			throw new IllegalStateException("Have not run BinarySvgCreate, Paths is still null");
		}
		return Paths;
	}
	
	/**
	 * Gets a batik DOM node that contains all of the filled-in shapes
	 * associated with this BinarySvgCreation.
	 * 
	 * @return
	 * @throws IllegalArgumentException
	 */
	/*AbstractNode getNode() throws IllegalArgumentException
	{
		// TODO: write this method. It should be small. The work should be done in run()
		return null;
	}*/
}
