package base;
import org.apache.batik.dom.*;
import java.io.*;
import java.util.ArrayList;
import java.awt.*;
import java.awt.geom.*;

import org.w3c.dom.*;
import org.apache.batik.svggen.*;


/**
 * Can combine AbstractNodes into one document and export that document to an SVG file.
 * 
 * @author Ian, Mike
 */
 
 
public class SvgMerge
{
	private SVGGraphics2D svg_generator;
	
	//Internal Function, used only within this class
	public void process_paths(Graphics2D g2d, ArrayList<GeneralPath> paths, Color color)
	{	
		this.svg_generator.setPaint(color);
		for (int i = 0; i < paths.size(); i++)
		{
			// Draw each path in the appropriate color
			
			this.svg_generator.fill(paths.get(i));
			// pois.get(0).getX();
		}
	}
	
	//Main use function
	public void setup()
	{
		//Setup the backend stuff for Batik
        DOMImplementation dom_imp = GenericDOMImplementation.getDOMImplementation();
        String str = "http://www.w3.org/2000/svg";
        Document document = dom_imp.createDocument(str, "svg", null);
        SVGGraphics2D svg = new SVGGraphics2D(document);
		this.svg_generator = svg;
	}
	
	public void addPaths(ArrayList<GeneralPath> paths, Color color)
	{
		this.process_paths(this.svg_generator, paths, color);
	}
	
	public void write_SVG()
	{	
	try {
		FileWriter fWriter = new FileWriter("test.svg");
		PrintWriter pWriter = new PrintWriter(fWriter);
		this.svg_generator.stream(pWriter);
		pWriter.close();
	}
	catch (IOException e) { 
		System.err.println(e.toString());
	}
	}
	//Depreciated functions below
	
	/**
	 * Adds the node to the document.
	 * 
	 * @param node the node to add to the document.
	 */
	/*public void addNode(AbstractNode node)
	{
		// TODO: implement this.
	}*/
	
	/**
	 * Exports the document to an SVG file
	 * 
	 * @param filePath the file path and name
	 */
	/*public void exportDocument(String filePath)
	{
		// TODO: implement this.
	}*/
}
