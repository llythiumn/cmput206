package base;
import java.util.ArrayList;

import filters.LineMaxFilter;
import filters.MagnitudeFilter;
import filters.XFilter;
import filters.YFilter;
import ij.IJ;
import ij.ImagePlus;
import ij.process.*;

/**
 * Represents the process of locating points of interest in an image.
 * (POI stands for Point Of Interest)
 * 
 * @author James
 * 
 * TODO: fix all references to ArrayList<Point<Integer>>
 */
public class PoiLocation extends VectorImageProcess
{

	//private ArrayList<Point<Float>> list = null;
	private ArrayList<Point<Integer>> list = null;
	private FloatProcessor       magnitude = null;

	/**
	 * Creates a PoiLocation associated with the image represented by
	 * the given FloatProcessor, which specifies angles, and the given
	 * ImageProcessor, which specifies magnitudes.
	 * 
	 * @param ip the ImageProcessor associated with this VectorImageProcess
	 *           and specifying magnitudes. This should be the crisp edge image.
	 * @param angles the FloatProcessor associated with this VectorImageProcess
	 *               and specifying angles in radians. These should be the
	 *               blurred angles.
	 */
	public PoiLocation(ImageProcessor ip, FloatProcessor angles)
	{
		super(ip, angles);
	}

	/**
	 * Locates the points of interest
	 */
	@Override
	public void run()
	{
		// Set up all proceedures
		FloatProcessor X, Y;
		XFilter Xfilt = new XFilter();
		YFilter Yfilt = new YFilter();
		MagnitudeFilter Magnitudefilt = new MagnitudeFilter();
			// Setting Threshold
		LineMaxFilter lineMaxFilter = new LineMaxFilter(0.5f);
		// execute the filter
		X = Xfilt.run(angles);
		Y = Yfilt.run(angles);
		// calculate the magnitude image
		magnitude = Magnitudefilt.run(X, Y);
		//use the magnitude with lines to find poi
		lineMaxFilter.run(magnitude, ip);
		list = lineMaxFilter.getPoints();
		IJ.error(String.valueOf(list.size()));
		
	}

	/**
	 * Gets the points of interest located by the call to run()
	 * 
	 * @return the points of interest
	 * @throws IllegalStateException thrown if this method is called before run()
	 */
	//public ArrayList<Point<Float>> getPoints() throws IllegalStateException
	public ArrayList<Point<Integer>> getPoints() throws IllegalStateException
	{
		// TODO: write this method. It should be small. The work should be done in run()
		if (list == null) {
			throw new IllegalStateException("Called getPoints before calling run");
		}
		return list;
	}

	/**
	 * Gets the points of interest located by the call to run()
	 * 
	 * @return the points of interest
	 * @throws IllegalStateException thrown if this method is called before run()
	 */
	//public ArrayList<Point<Float>> getPoints() throws IllegalStateException
	public FloatProcessor getPoi() throws IllegalStateException
	{
		// TODO: write this method. It should be small. The work should be done in run()
		if (magnitude == null) {
			throw new IllegalStateException("Called getPoi before calling run");
		}
		return magnitude;
	}
}
