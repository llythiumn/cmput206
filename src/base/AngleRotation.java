package base;
import ij.process.*;

/**
 * A simple rotation of angles by a given number of radians
 * 
 * @author James Helberg
 * 
 * @deprecated filters/AngleFilter.java now accounts for angle rotation by 90 degrees clockwise
 */
public class AngleRotation extends VectorImageProcess
{

	/**
	 * Creates an AngleRotation associated with the image represented by the
	 * given FloatProcessor, which specifies angles, and the given
	 * ImageProcessor, which specifies magnitudes.
	 * 
	 * @param ip the ImageProcessor associated with this VectorImageProcess
	 *           and specifying magnitudes
	 * @param angles the FloatProcessor associated with this VectorImageProcess
	 *               and specifying angles in radians
	 * @param radians the radians to rotate by.
	 */
	public AngleRotation(ImageProcessor ip, FloatProcessor angles, float radians)
	{
		super(ip, angles);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Rotates the angles.
	 */
	@Override
	public void run()
	{
		// TODO Auto-generated method stub

	}

}
