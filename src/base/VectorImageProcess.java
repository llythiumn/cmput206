package base;
import java.awt.Color;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;

import ij.process.*;

/**
 * Represents any process performed on an image where each pixel is
 * represented by an angle and a magnitude.
 * 
 * @author Tim, James
 */
public abstract class VectorImageProcess extends ImageProcess
{
	protected FloatProcessor angles;

	/**
	 * Creates a VectorImageProcess associated with the image represented by
	 * the given FloatProcessor, which specifies angles, and the given
	 * ImageProcessor, which specifies magnitudes.
	 * 
	 * NOTE: It may be useful to modify this constructor to make a copy of the
	 * FloatProcessor, so that run() doesn't modify the original. Maybe.
	 * 
	 * @param ip the ImageProcessor associated with this VectorImageProcess
	 *           and specifying magnitudes
	 * @param angles the FloatProcessor associated with this VectorImageProcess
	 *               and specifying angles in radians
	 */
	protected VectorImageProcess(ImageProcessor ip, FloatProcessor angles)
	{
		super(ip);
		this.angles = angles;
	}
	
	/**
	 * Modifies the image represented by this ImageProcess's ImageProcessor in
	 * some way.
	 */
	public abstract void run();

	/**
	 * Gets the FloatProcessor associated with this VectorImageProcess and
	 * specifying angles in radians
	 * 
	 * @return the FloatProcessor associated with this VectorImageProcess and
	 * specifying angles in radians
	 */
	public FloatProcessor getAngles()
	{
		return this.angles;
	}
}
