package gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import base.*;

@SuppressWarnings("serial")
public class ColorSelectionPanel extends JPanel implements ActionListener, DocumentListener, ChangeListener
{
	private ColorButton colorButton;
	private IntTextField textField;
	private JSlider slider;
	
	public ColorSelectionPanel()
	{
		this.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		
		this.colorButton = new ColorButton();
		this.colorButton.setPreferredSize(new Dimension(20, 20));
		this.colorButton.setSize(this.colorButton.getPreferredSize());
		this.colorButton.setActionCommand("ColorPick");
		this.colorButton.addActionListener(this);
		
		JLabel label = new JLabel("Tolerance:");
		
		int defaultTolerance = 15;
		this.textField = new IntTextField(defaultTolerance, 3);
		this.textField.getDocument().addDocumentListener(this);
		
		this.slider = new JSlider();
		this.slider.setExtent(1);
		this.slider.setMaximum(Constants.MaxTolerance+1);
		this.slider.setMinimum(0);
		this.slider.setValue(defaultTolerance);
		this.slider.addChangeListener(this);
		
		JButton closeButton = new JButton("X");
		closeButton.addActionListener(this);
		
		int colorButtonHeight = this.colorButton.getPreferredSize().height;
		int labelHeight = UIManager.getUI(label).getPreferredSize(label).height;
		int textFieldHeight = this.textField.getPreferredSize().height;
		try
		{
			textFieldHeight = UIManager.getUI(this.textField).getPreferredSize(this.textField).height;
		}
		catch (Exception e) {}
		int sliderHeight = this.slider.getPreferredSize().height;
		try
		{
			sliderHeight = UIManager.getUI(this.slider).getPreferredSize(this.slider).height;
		}
		catch (Exception e) {}
		int closeButtonHeight = UIManager.getUI(closeButton).getPreferredSize(closeButton).height;
		closeButton.setPreferredSize(new Dimension(closeButtonHeight, closeButtonHeight));
		
		JComponent tallestComponent = this.colorButton;
		int tallestComponentHeight = colorButtonHeight;
		if (labelHeight > colorButtonHeight)
		{
			tallestComponent = label;
			tallestComponentHeight = labelHeight;
		}
		if (textFieldHeight > tallestComponentHeight)
		{
			tallestComponent = this.textField;
			tallestComponentHeight = textFieldHeight;
		}
		if (sliderHeight > tallestComponentHeight)
		{
			tallestComponent = this.slider;
			tallestComponentHeight = sliderHeight;
		}
		if (closeButtonHeight > tallestComponentHeight)
		{
			tallestComponent = closeButton;
			tallestComponentHeight = closeButtonHeight;
		}
		
		SpringLayout layout = new SpringLayout();
		layout.putConstraint(SpringLayout.NORTH, tallestComponent, Constants.InteriorSpacing, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, this.colorButton, Constants.InteriorSpacing, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.SOUTH, this, Constants.InteriorSpacing, SpringLayout.SOUTH, tallestComponent);
		layout.putConstraint(SpringLayout.WEST, label, Constants.UnrelatedControlSpacing, SpringLayout.EAST, this.colorButton);
		layout.putConstraint(SpringLayout.WEST, this.textField, Constants.RelatedControlSpacing, SpringLayout.EAST, label);
		layout.putConstraint(SpringLayout.WEST, this.slider, Constants.RelatedControlSpacing, SpringLayout.EAST, this.textField);
		layout.putConstraint(SpringLayout.WEST, closeButton, Constants.UnrelatedControlSpacing, SpringLayout.EAST, this.slider);
		layout.putConstraint(SpringLayout.EAST, this, Constants.InteriorSpacing, SpringLayout.EAST, closeButton);
		
		if (this.colorButton != tallestComponent)
			layout.putConstraint(SpringLayout.NORTH, this.colorButton, (tallestComponentHeight - colorButtonHeight)/2, SpringLayout.NORTH, tallestComponent);
		if (label != tallestComponent)
			layout.putConstraint(SpringLayout.NORTH, label, (tallestComponentHeight - labelHeight)/2, SpringLayout.NORTH, tallestComponent);
		if (this.textField != tallestComponent)
			layout.putConstraint(SpringLayout.NORTH, this.textField, (tallestComponentHeight - textFieldHeight)/2, SpringLayout.NORTH, tallestComponent);
		if (this.slider != tallestComponent)
			layout.putConstraint(SpringLayout.NORTH, this.slider, (tallestComponentHeight - sliderHeight)/2, SpringLayout.NORTH, tallestComponent);
		if (closeButton != tallestComponent)
			layout.putConstraint(SpringLayout.NORTH, closeButton, (tallestComponentHeight - closeButtonHeight)/2, SpringLayout.NORTH, tallestComponent);
		
		this.add(this.colorButton);
		this.add(label);
		this.add(this.textField);
		this.add(this.slider);
		this.add(closeButton);
		
		this.setLayout(layout);
	}
	
	public Color getColor()
	{
		return this.colorButton.getColor();
	}
	
	public void setColor(Color color)
	{
		this.colorButton.setColor(color);
	}
	
	public int getTolerance()
	{
		return this.slider.getValue();
	}
	
	public ApproximateColor getApproximateColor()
	{
		return new ApproximateColor(this.getColor().getRGB(), this.getTolerance());
	}
	
	public void addActionListener(ActionListener l)
	{
		this.listenerList.add(ActionListener.class, l);
	}
	
	public void removeActionListener(ActionListener l)
	{
		this.listenerList.remove(ActionListener.class, l);
	}
	
	protected void fireActionEvent(ActionEvent source)
	{
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length-2; i>=0; i-=2) {
			if (listeners[i]==ActionListener.class) {
				// Create the event:
				ActionEvent e = new ActionEvent(this, source.getID(), source.getActionCommand(), source.getWhen(), source.getModifiers());
				((ActionListener)listeners[i+1]).actionPerformed(e);
			}
		}
	}
	
	public void addChangeListener(ChangeListener l)
	{
		this.listenerList.add(ChangeListener.class, l);
	}
	
	public void removeChangeListener(ChangeListener l)
	{
		this.listenerList.remove(ChangeListener.class, l);
	}
	
	protected void fireChangeEvent()
	{
		// Guaranteed to return a non-null array
		Object[] listeners = listenerList.getListenerList();
		// Process the listeners last to first, notifying
		// those that are interested in this event
		for (int i = listeners.length-2; i>=0; i-=2) {
			if (listeners[i]==ChangeListener.class) {
				// Create the event:
				ChangeEvent e = new ChangeEvent(this);
				((ChangeListener)listeners[i+1]).stateChanged(e);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		this.fireActionEvent(e);
	}

	@Override
	public void changedUpdate(DocumentEvent e)
	{
		// do nothing
	}

	@Override
	public void insertUpdate(DocumentEvent e)
	{
		updateSlider();
	}

	@Override
	public void removeUpdate(DocumentEvent e)
	{
		updateSlider();
	}
	
	private void updateSlider()
	{
		if (this.textField.isValid())
		{
			this.slider.removeChangeListener(this);
			this.slider.setValue(this.textField.getValue());
			this.slider.addChangeListener(this);
			this.fireChangeEvent();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		this.textField.getDocument().removeDocumentListener(this);
		this.textField.setText(new Integer(this.slider.getValue()).toString());
		this.textField.getDocument().addDocumentListener(this);
		this.fireChangeEvent();
	}
}
