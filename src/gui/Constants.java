package gui;

public class Constants
{
	/**
	 * The spacing, in pixels, between a container's edges and the controls nearest
	 * those edges
	 */
	public final static int InteriorSpacing = 8;
	
	/**
	 * The spacing, in pixels, between two closely related controls, such as a text
	 * field and its label.
	 */
	public final static int RelatedControlSpacing = 6;
	
	/**
	 * The spacing, in pixels, between two controls that aren't directly related.
	 */
	public final static int UnrelatedControlSpacing = 10;
	
	/**
	 * The maximum tolerance radius in hue-saturation-brightness space.
	 */
	public final static int MaxTolerance = 150;
}
