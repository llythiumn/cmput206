package gui;

import ij.*;
import ij.plugin.frame.*;
import ij.process.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import base.ApproximateColor;

/**
 * A java swing frame that allows the ImageJ plugin user to select approximate colors.
 * 
 * @author Tim
 */
@SuppressWarnings("serial")
public class ColorSelectionFrame extends PlugInFrame implements ActionListener, ChangeListener, MouseListener
{
	private Vector<ColorSelectionPanel> colorSelectionPanels = new Vector<ColorSelectionPanel>();
	private ImagePlus imp;
	private PreviewThread previewThread;
	private ImageDisplay originalImage, previewImage;
	private JButton addButton;
	private boolean colorPickerMode = false;
	private ColorSelectionPanel colorPickerSource = null;
	private Cursor colorPickerCursor;
	private boolean isCancelled = true;
	private Object notifyOnClose;
	
	/**
	 * Creates the frame with a default title and the given image
	 * 
	 * @param imp the image to select colours from.
	 * @param notifyOnClose an object to notify when this frame closes
	 * @throws HeadlessException if GraphicsEnvironment.isHeadless() returns true.
	 */
	public ColorSelectionFrame(ImagePlus imp, Object notifyOnClose) throws HeadlessException
	{
		super("Select Vectorization Colours");
		this.imp = imp;
		this.notifyOnClose = notifyOnClose;
		
		this.addWindowListener(this);
		
		// create the eyedropper cursor
		BufferedImage cursorImage = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
		cursorImage.setRGB(0, 0, 0);
		cursorImage.setRGB(0, 1, 0);
		cursorImage.setRGB(0, 2, 0);
		cursorImage.setRGB(0, 3, 0);
		cursorImage.setRGB(0, 4, 0);
		cursorImage.setRGB(0, 5, 0);
		cursorImage.setRGB(0, 6, 0);
		cursorImage.setRGB(0, 7, 0);
		cursorImage.setRGB(0, 8, 0);
		cursorImage.setRGB(0, 9, 0);
		cursorImage.setRGB(0, 10, 0);
		cursorImage.setRGB(0, 11, 0);
		cursorImage.setRGB(0, 12, 0);
		cursorImage.setRGB(0, 13, 0);
		cursorImage.setRGB(0, 14, -5000269);
		cursorImage.setRGB(0, 15, 117440512);
		cursorImage.setRGB(1, 0, 0);
		cursorImage.setRGB(1, 1, 0);
		cursorImage.setRGB(1, 2, 0);
		cursorImage.setRGB(1, 3, 0);
		cursorImage.setRGB(1, 4, 0);
		cursorImage.setRGB(1, 5, 0);
		cursorImage.setRGB(1, 6, 0);
		cursorImage.setRGB(1, 7, 0);
		cursorImage.setRGB(1, 8, 0);
		cursorImage.setRGB(1, 9, 0);
		cursorImage.setRGB(1, 10, 0);
		cursorImage.setRGB(1, 11, 0);
		cursorImage.setRGB(1, 12, -9145228);
		cursorImage.setRGB(1, 13, -5131855);
		cursorImage.setRGB(1, 14, -592138);
		cursorImage.setRGB(1, 15, -15856114);
		cursorImage.setRGB(2, 0, 0);
		cursorImage.setRGB(2, 1, 0);
		cursorImage.setRGB(2, 2, 0);
		cursorImage.setRGB(2, 3, 0);
		cursorImage.setRGB(2, 4, 0);
		cursorImage.setRGB(2, 5, 0);
		cursorImage.setRGB(2, 6, 0);
		cursorImage.setRGB(2, 7, 0);
		cursorImage.setRGB(2, 8, 0);
		cursorImage.setRGB(2, 9, 0);
		cursorImage.setRGB(2, 10, 0);
		cursorImage.setRGB(2, 11, -6118750);
		cursorImage.setRGB(2, 12, -1);
		cursorImage.setRGB(2, 13, -723724);
		cursorImage.setRGB(2, 14, -15921907);
		cursorImage.setRGB(2, 15, 754974720);
		cursorImage.setRGB(3, 0, 0);
		cursorImage.setRGB(3, 1, 0);
		cursorImage.setRGB(3, 2, 0);
		cursorImage.setRGB(3, 3, 0);
		cursorImage.setRGB(3, 4, 0);
		cursorImage.setRGB(3, 5, 0);
		cursorImage.setRGB(3, 6, 0);
		cursorImage.setRGB(3, 7, 0);
		cursorImage.setRGB(3, 8, 0);
		cursorImage.setRGB(3, 9, 0);
		cursorImage.setRGB(3, 10, -6118750);
		cursorImage.setRGB(3, 11, -1);
		cursorImage.setRGB(3, 12, -657931);
		cursorImage.setRGB(3, 13, -5066062);
		cursorImage.setRGB(3, 14, -15921907);
		cursorImage.setRGB(3, 15, 754974720);
		cursorImage.setRGB(4, 0, 0);
		cursorImage.setRGB(4, 1, 0);
		cursorImage.setRGB(4, 2, 0);
		cursorImage.setRGB(4, 3, 0);
		cursorImage.setRGB(4, 4, 0);
		cursorImage.setRGB(4, 5, 0);
		cursorImage.setRGB(4, 6, 0);
		cursorImage.setRGB(4, 7, 0);
		cursorImage.setRGB(4, 8, 0);
		cursorImage.setRGB(4, 9, -6250336);
		cursorImage.setRGB(4, 10, -1);
		cursorImage.setRGB(4, 11, -723724);
		cursorImage.setRGB(4, 12, -5066062);
		cursorImage.setRGB(4, 13, -15987700);
		cursorImage.setRGB(4, 14, 872415232);
		cursorImage.setRGB(4, 15, 587202560);
		cursorImage.setRGB(5, 0, 0);
		cursorImage.setRGB(5, 1, 0);
		cursorImage.setRGB(5, 2, 0);
		cursorImage.setRGB(5, 3, 0);
		cursorImage.setRGB(5, 4, 0);
		cursorImage.setRGB(5, 5, 0);
		cursorImage.setRGB(5, 6, 0);
		cursorImage.setRGB(5, 7, 0);
		cursorImage.setRGB(5, 8, -6118750);
		cursorImage.setRGB(5, 9, -1);
		cursorImage.setRGB(5, 10, -657931);
		cursorImage.setRGB(5, 11, -5131855);
		cursorImage.setRGB(5, 12, -15987700);
		cursorImage.setRGB(5, 13, 872415232);
		cursorImage.setRGB(5, 14, 637534208);
		cursorImage.setRGB(5, 15, 285212672);
		cursorImage.setRGB(6, 0, 0);
		cursorImage.setRGB(6, 1, 0);
		cursorImage.setRGB(6, 2, 0);
		cursorImage.setRGB(6, 3, 0);
		cursorImage.setRGB(6, 4, 0);
		cursorImage.setRGB(6, 5, 0);
		cursorImage.setRGB(6, 6, 0);
		cursorImage.setRGB(6, 7, -6316129);
		cursorImage.setRGB(6, 8, -1);
		cursorImage.setRGB(6, 9, -657931);
		cursorImage.setRGB(6, 10, -5066062);
		cursorImage.setRGB(6, 11, -15921907);
		cursorImage.setRGB(6, 12, 872415232);
		cursorImage.setRGB(6, 13, 637534208);
		cursorImage.setRGB(6, 14, 285212672);
		cursorImage.setRGB(6, 15, 50331648);
		cursorImage.setRGB(7, 0, 0);
		cursorImage.setRGB(7, 1, 0);
		cursorImage.setRGB(7, 2, 0);
		cursorImage.setRGB(7, 3, 0);
		cursorImage.setRGB(7, 4, -14869210);
		cursorImage.setRGB(7, 5, 117440512);
		cursorImage.setRGB(7, 6, -6579293);
		cursorImage.setRGB(7, 7, -1);
		cursorImage.setRGB(7, 8, -723724);
		cursorImage.setRGB(7, 9, -5066062);
		cursorImage.setRGB(7, 10, -15856114);
		cursorImage.setRGB(7, 11, 872415232);
		cursorImage.setRGB(7, 12, 637534208);
		cursorImage.setRGB(7, 13, 285212672);
		cursorImage.setRGB(7, 14, 50331648);
		cursorImage.setRGB(7, 15, 0);
		cursorImage.setRGB(8, 0, 0);
		cursorImage.setRGB(8, 1, 0);
		cursorImage.setRGB(8, 2, 0);
		cursorImage.setRGB(8, 3, -14539720);
		cursorImage.setRGB(8, 4, -7236157);
		cursorImage.setRGB(8, 5, -15066327);
		cursorImage.setRGB(8, 6, -7565658);
		cursorImage.setRGB(8, 7, -7500135);
		cursorImage.setRGB(8, 8, -9934484);
		cursorImage.setRGB(8, 9, -15921907);
		cursorImage.setRGB(8, 10, 872415232);
		cursorImage.setRGB(8, 11, 637534208);
		cursorImage.setRGB(8, 12, 285212672);
		cursorImage.setRGB(8, 13, 50331648);
		cursorImage.setRGB(8, 14, 0);
		cursorImage.setRGB(8, 15, 0);
		cursorImage.setRGB(9, 0, 0);
		cursorImage.setRGB(9, 1, 0);
		cursorImage.setRGB(9, 2, -15000537);
		cursorImage.setRGB(9, 3, -7433529);
		cursorImage.setRGB(9, 4, -12301146);
		cursorImage.setRGB(9, 5, -14210476);
		cursorImage.setRGB(9, 6, -15132118);
		cursorImage.setRGB(9, 7, -10723720);
		cursorImage.setRGB(9, 8, -16053235);
		cursorImage.setRGB(9, 9, 872415232);
		cursorImage.setRGB(9, 10, 637534208);
		cursorImage.setRGB(9, 11, 285212672);
		cursorImage.setRGB(9, 12, 50331648);
		cursorImage.setRGB(9, 13, 0);
		cursorImage.setRGB(9, 14, 0);
		cursorImage.setRGB(9, 15, 0);
		cursorImage.setRGB(10, 0, 0);
		cursorImage.setRGB(10, 1, 0);
		cursorImage.setRGB(10, 2, -6315327);
		cursorImage.setRGB(10, 3, -15197644);
		cursorImage.setRGB(10, 4, -14341531);
		cursorImage.setRGB(10, 5, -15592144);
		cursorImage.setRGB(10, 6, -15395026);
		cursorImage.setRGB(10, 7, -15066327);
		cursorImage.setRGB(10, 8, 872415232);
		cursorImage.setRGB(10, 9, 637534208);
		cursorImage.setRGB(10, 10, 285212672);
		cursorImage.setRGB(10, 11, 50331648);
		cursorImage.setRGB(10, 12, 0);
		cursorImage.setRGB(10, 13, 0);
		cursorImage.setRGB(10, 14, 0);
		cursorImage.setRGB(10, 15, 0);
		cursorImage.setRGB(11, 0, 0);
		cursorImage.setRGB(11, 1, 0);
		cursorImage.setRGB(11, 2, -15197654);
		cursorImage.setRGB(11, 3, -7828019);
		cursorImage.setRGB(11, 4, -15460552);
		cursorImage.setRGB(11, 5, -15197118);
		cursorImage.setRGB(11, 6, -15460817);
		cursorImage.setRGB(11, 7, -15197653);
		cursorImage.setRGB(11, 8, -14868955);
		cursorImage.setRGB(11, 9, 469762048);
		cursorImage.setRGB(11, 10, 117440512);
		cursorImage.setRGB(11, 11, 0);
		cursorImage.setRGB(11, 12, 0);
		cursorImage.setRGB(11, 13, 0);
		cursorImage.setRGB(11, 14, 0);
		cursorImage.setRGB(11, 15, 0);
		cursorImage.setRGB(12, 0, 0);
		cursorImage.setRGB(12, 1, -15131863);
		cursorImage.setRGB(12, 2, -7630646);
		cursorImage.setRGB(12, 3, -12432728);
		cursorImage.setRGB(12, 4, -14341539);
		cursorImage.setRGB(12, 5, -14934211);
		cursorImage.setRGB(12, 6, -14802885);
		cursorImage.setRGB(12, 7, -14473929);
		cursorImage.setRGB(12, 8, 822083584);
		cursorImage.setRGB(12, 9, 469762048);
		cursorImage.setRGB(12, 10, 117440512);
		cursorImage.setRGB(12, 11, 0);
		cursorImage.setRGB(12, 12, 0);
		cursorImage.setRGB(12, 13, 0);
		cursorImage.setRGB(12, 14, 0);
		cursorImage.setRGB(12, 15, 0);
		cursorImage.setRGB(13, 0, -15000537);
		cursorImage.setRGB(13, 1, -7433274);
		cursorImage.setRGB(13, 2, -12235611);
		cursorImage.setRGB(13, 3, -14276267);
		cursorImage.setRGB(13, 4, -14934476);
		cursorImage.setRGB(13, 5, -9078361);
		cursorImage.setRGB(13, 6, -14934745);
		cursorImage.setRGB(13, 7, 872415232);
		cursorImage.setRGB(13, 8, 637534208);
		cursorImage.setRGB(13, 9, 285212672);
		cursorImage.setRGB(13, 10, 50331648);
		cursorImage.setRGB(13, 11, 0);
		cursorImage.setRGB(13, 12, 0);
		cursorImage.setRGB(13, 13, 0);
		cursorImage.setRGB(13, 14, 0);
		cursorImage.setRGB(13, 15, 0);
		cursorImage.setRGB(14, 0, -15000537);
		cursorImage.setRGB(14, 1, -15132114);
		cursorImage.setRGB(14, 2, -13881510);
		cursorImage.setRGB(14, 3, -15197654);
		cursorImage.setRGB(14, 4, -8289372);
		cursorImage.setRGB(14, 5, 872415232);
		cursorImage.setRGB(14, 6, 754974720);
		cursorImage.setRGB(14, 7, 587202560);
		cursorImage.setRGB(14, 8, 285212672);
		cursorImage.setRGB(14, 9, 50331648);
		cursorImage.setRGB(14, 10, 0);
		cursorImage.setRGB(14, 11, 0);
		cursorImage.setRGB(14, 12, 0);
		cursorImage.setRGB(14, 13, 0);
		cursorImage.setRGB(14, 14, 0);
		cursorImage.setRGB(14, 15, 0);
		cursorImage.setRGB(15, 0, 167772160);
		cursorImage.setRGB(15, 1, -14276808);
		cursorImage.setRGB(15, 2, -14737361);
		cursorImage.setRGB(15, 3, -5723458);
		cursorImage.setRGB(15, 4, 872415232);
		cursorImage.setRGB(15, 5, 637534208);
		cursorImage.setRGB(15, 6, 352321536);
		cursorImage.setRGB(15, 7, 167772160);
		cursorImage.setRGB(15, 8, 50331648);
		cursorImage.setRGB(15, 9, 0);
		cursorImage.setRGB(15, 10, 0);
		cursorImage.setRGB(15, 11, 0);
		cursorImage.setRGB(15, 12, 0);
		cursorImage.setRGB(15, 13, 0);
		cursorImage.setRGB(15, 14, 0);
		cursorImage.setRGB(15, 15, 0);
		
		this.colorPickerCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(1,14), "Eyedropper");
		
		JLabel originalLabel = new JLabel("Original:");
		JLabel previewLabel = new JLabel("Selected Colours:");
		this.addButton = new JButton("+");
		this.addButton.addActionListener(this);
		int addButtonHeight = UIManager.getUI(this.addButton).getPreferredSize(this.addButton).height;
		this.addButton.setPreferredSize(new Dimension(addButtonHeight, addButtonHeight));
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);
		JButton okButton = new JButton("OK");
		okButton.addActionListener(this);
		
		this.setPreferredSize(new Dimension(1000, 730));
		int imageDisplayHeight = (700 - Constants.InteriorSpacing * 2 - Constants.RelatedControlSpacing * 2 - UIManager.getUI(originalLabel).getPreferredSize(originalLabel).height * 2)/2;
		int imageDisplayWidth = imp.getWidth() * imageDisplayHeight / imp.getHeight();
		
		originalImage = new ImageDisplay();
		originalImage.setPreferredSize(new Dimension(imageDisplayWidth, imageDisplayHeight));
		originalImage.setImage(imp.getImage());
		
		previewImage = new ImageDisplay();
		previewImage.setPreferredSize(new Dimension(imageDisplayWidth, imageDisplayHeight));
		Insets insets = previewImage.getInsets();
		BufferedImage image = new BufferedImage(imageDisplayWidth - insets.left - insets.right, imageDisplayHeight - insets.top - insets.bottom, BufferedImage.TYPE_BYTE_BINARY);
		Graphics g = image.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, image.getWidth(), image.getHeight());
		previewImage.setImage(image);
		
		ColorSelectionPanel panel = new ColorSelectionPanel();
		panel.addChangeListener(this);
		panel.addActionListener(this);
		this.colorSelectionPanels.add(panel);
		
		SpringLayout layout = new SpringLayout();
		layout.putConstraint(SpringLayout.NORTH, originalLabel, Constants.InteriorSpacing, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, originalLabel, Constants.InteriorSpacing, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, originalImage, Constants.RelatedControlSpacing, SpringLayout.SOUTH, originalLabel);
		layout.putConstraint(SpringLayout.WEST, originalImage, 0, SpringLayout.WEST, originalLabel);
		layout.putConstraint(SpringLayout.EAST, originalImage, imageDisplayWidth, SpringLayout.WEST, originalImage);
		layout.putConstraint(SpringLayout.NORTH, previewLabel, Constants.UnrelatedControlSpacing, SpringLayout.SOUTH, originalImage);
		layout.putConstraint(SpringLayout.WEST, previewLabel, 0, SpringLayout.WEST, originalImage);
		layout.putConstraint(SpringLayout.NORTH, previewImage, Constants.RelatedControlSpacing, SpringLayout.SOUTH, previewLabel);
		layout.putConstraint(SpringLayout.WEST, previewImage, 0, SpringLayout.WEST, previewLabel);
		layout.putConstraint(SpringLayout.WEST, panel, Constants.UnrelatedControlSpacing, SpringLayout.EAST, originalImage);
		layout.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, originalImage);
		layout.putConstraint(SpringLayout.NORTH, addButton, Constants.RelatedControlSpacing, SpringLayout.SOUTH, panel);
		layout.putConstraint(SpringLayout.EAST, addButton, -Constants.InteriorSpacing, SpringLayout.EAST, panel);
		layout.putConstraint(SpringLayout.EAST, okButton, -Constants.InteriorSpacing, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, okButton, -Constants.InteriorSpacing, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.EAST, cancelButton, -Constants.RelatedControlSpacing, SpringLayout.WEST, okButton);
		layout.putConstraint(SpringLayout.SOUTH, cancelButton, -Constants.InteriorSpacing, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.EAST, this, Constants.InteriorSpacing, SpringLayout.EAST, panel);
		
		this.add(originalLabel);
		this.add(originalImage);
		this.add(previewLabel);
		this.add(previewImage);
		this.add(panel);
		this.add(addButton);
		this.add(okButton);
		this.add(cancelButton);
		
		this.setLayout(layout);
	}
	
	public boolean wasCancelled()
	{
		return this.isCancelled;
	}

	public ApproximateColor[] getColors()
	{
		ApproximateColor[] colors = new ApproximateColor[this.colorSelectionPanels.size()];
		for (int i = 0; i < colors.length; i++)
		{
			colors[i] = this.colorSelectionPanels.get(i).getApproximateColor();
		}
		return colors;
	}
	
	@Override
	public void windowClosed(WindowEvent e)
	{
		super.windowClosed(e);
		synchronized (this.notifyOnClose)
		{
			this.notifyOnClose.notify();
		}
	}

	@Override
	public void stateChanged(ChangeEvent e)
	{
		redrawPreview();
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (this.colorPickerMode)
		{
			this.colorPickerMode = false;
			this.setCursor(Cursor.getDefaultCursor());
			this.originalImage.removeMouseListener(this);
		}
		
		if (e.getActionCommand().equals("+"))
			this.addPanel();
		else if (e.getActionCommand().equalsIgnoreCase("X"))
			this.removePanel((ColorSelectionPanel)e.getSource());
		else if (e.getActionCommand().equalsIgnoreCase("OK"))
		{
			this.isCancelled = false;
			this.setVisible(false);
			synchronized (this.notifyOnClose)
			{
				this.notifyOnClose.notifyAll();
			}
		}
		else if (e.getActionCommand().equalsIgnoreCase("Cancel"))
		{
			this.setVisible(false);
			synchronized (this.notifyOnClose)
			{
				this.notifyOnClose.notifyAll();
			}
		}
		else
			this.enterColorPickerMode((ColorSelectionPanel)e.getSource());
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		Insets insets = this.originalImage.getInsets();
		if (e.getX() > insets.left && e.getX() < this.originalImage.getWidth() - insets.right && e.getY() > insets.top && e.getY() < this.originalImage.getHeight() - insets.bottom)
		{
			this.setCursor(Cursor.getDefaultCursor());
			this.originalImage.removeMouseListener(this);
			this.colorPickerMode = false;
			float xFraction = ((float)(e.getX() - insets.left)) / (float)(this.originalImage.getWidth() - insets.left - insets.right);
			float yFraction = ((float)(e.getY() - insets.top)) / (float)(this.originalImage.getHeight() - insets.top - insets.bottom);
			int x = (int)(((float)(imp.getWidth())) * xFraction);
			int y = (int)(((float)(imp.getHeight())) * yFraction);
			this.colorPickerSource.setColor(new Color(this.imp.getProcessor().get(x, y)));
			redrawPreview();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e)
	{
		// do nothing
	}

	@Override
	public void mouseExited(MouseEvent e)
	{
		// do nothing
	}

	@Override
	public void mousePressed(MouseEvent e)
	{
		// do nothing
	}

	@Override
	public void mouseReleased(MouseEvent e)
	{
		// do nothing
	}
	
	private void addPanel()
	{
		ColorSelectionPanel lastPanel = this.colorSelectionPanels.lastElement();
		ColorSelectionPanel panel = new ColorSelectionPanel();
		panel.addActionListener(this);
		panel.addChangeListener(this);
		this.colorSelectionPanels.add(panel);
		this.add(panel);
		
		SpringLayout layout = (SpringLayout)this.getLayout();
		layout.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.SOUTH, lastPanel);
		layout.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, lastPanel);
		layout.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, lastPanel);
		layout.putConstraint(SpringLayout.NORTH, this.addButton, Constants.RelatedControlSpacing, SpringLayout.SOUTH, panel);
		
		this.validate();
		panel.revalidate();
	}
	
	private void removePanel(ColorSelectionPanel panel)
	{
		ColorSelectionPanel previousPanel = null;
		ColorSelectionPanel nextPanel = null;
		int panelIndex = this.colorSelectionPanels.indexOf(panel);
		if (panelIndex < 0)
			return;
		if (panelIndex > 0)
			previousPanel = this.colorSelectionPanels.get(panelIndex - 1);
		if (panelIndex < this.colorSelectionPanels.size() - 1)
			nextPanel = this.colorSelectionPanels.get(panelIndex + 1);
		
		SpringLayout layout = (SpringLayout)this.getLayout();
		if (previousPanel == null && nextPanel == null)
			return; // don't allow removal of the last panel
		else
		{
			layout.removeLayoutComponent(panel);
			if (previousPanel == null)
			{
				layout.putConstraint(SpringLayout.NORTH, nextPanel, 0, SpringLayout.NORTH, this.originalImage);
				layout.putConstraint(SpringLayout.WEST, nextPanel, Constants.UnrelatedControlSpacing, SpringLayout.EAST, originalImage);
				layout.putConstraint(SpringLayout.EAST, this, Constants.InteriorSpacing, SpringLayout.EAST, nextPanel);
				layout.putConstraint(SpringLayout.EAST, addButton, -Constants.InteriorSpacing, SpringLayout.EAST, nextPanel);
			}
			else if (nextPanel == null)
				layout.putConstraint(SpringLayout.NORTH, this.addButton, Constants.RelatedControlSpacing, SpringLayout.SOUTH, previousPanel);
			else
			{
				layout.putConstraint(SpringLayout.NORTH, nextPanel, 0, SpringLayout.SOUTH, previousPanel);
				layout.putConstraint(SpringLayout.EAST, nextPanel, 0, SpringLayout.EAST, previousPanel);
				layout.putConstraint(SpringLayout.WEST, nextPanel, 0, SpringLayout.WEST, previousPanel);
			}
		}
		
		panel.removeChangeListener(this);
		panel.removeActionListener(this);
		this.remove(panel);
		this.colorSelectionPanels.remove(panelIndex);
		this.validate();
		this.repaint();
		redrawPreview();
	}
	
	private void enterColorPickerMode(ColorSelectionPanel panel)
	{
		this.colorPickerMode = true;
		this.colorPickerSource = panel;
		this.setCursor(this.colorPickerCursor);
		this.originalImage.addMouseListener(this);
	}
	
	private void redrawPreview()
	{
		if (this.previewThread != null)
			this.previewThread.stopIfRunning();
		this.previewThread = new PreviewThread(this.imp, this.getColors(), this.previewImage);
		this.previewThread.start();
	}
}
