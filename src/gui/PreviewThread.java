package gui;

import java.awt.*;
import ij.*;
import ij.process.*;
import base.ApproximateColor;

public class PreviewThread extends Thread
{
	private ImagePlus imp;
	private ApproximateColor[] colors;
	private ImageDisplay display;
	private Object runningLock = new Object();
	private boolean running = false;
	
	public PreviewThread(ImagePlus imp, ApproximateColor[] colors, ImageDisplay display)
	{
		this.imp = imp;
		this.colors = colors;
		this.display = display;
	}
	
	@SuppressWarnings("deprecation")
	public void stopIfRunning()
	{
		synchronized (runningLock)
		{
			if (this.running)
				this.stop();
		}
	}
	
	public void run()
	{
		synchronized (runningLock)
		{
			this.running = true;
		}
		
		Insets insets = this.display.getInsets();
		ImageProcessor previewIp = this.imp.getProcessor().resize(this.display.getWidth() - insets.left - insets.right, this.display.getHeight() - insets.top - insets.bottom, true);
		for (int i = 0; i < previewIp.getWidth(); i++)
		{
			for (int j = 0; j < previewIp.getHeight(); j++)
			{
				boolean blank = true;
				for (int k = 0; k < this.colors.length; k++)
				{
					Color color = new Color(previewIp.get(i, j));
					if (this.colors[k].matches(color))
					{
						previewIp.setColor(this.colors[k]);
						previewIp.drawPixel(i, j);
						blank = false;
					}
				}
				if (blank)
				{
					previewIp.setColor(Color.WHITE);
					previewIp.drawPixel(i, j);
				}
			}
		}
		this.display.setImage(previewIp.getBufferedImage());
		
		synchronized (runningLock)
		{
			this.running = false;
		}
	}
}
