package gui;

import javax.swing.*;
import javax.swing.text.*;

@SuppressWarnings("serial")
public class IntTextField extends JTextField {
	  public IntTextField(int defval, int size) {
	    super("" + defval, size);
	  }

	  protected Document createDefaultModel() {
	    return new IntTextDocument();
	  }

	  public boolean isValid() {
	    try {
	      int test = Integer.parseInt(getText());
	      if (test >= 0 && test <= Constants.MaxTolerance)
	    	  return true;
	      else
	    	  return false;
	    } catch (NumberFormatException e) {
	      return false;
	    } catch (NullPointerException npe) {
	      return false;
	    }
	  }

	  public int getValue() {
	    try {
	      return Integer.parseInt(getText());
	    } catch (NumberFormatException e) {
	      return 0;
	    }
	  }
	  
	  public class IntTextDocument extends PlainDocument {
	    public void insertString(int offs, String str, AttributeSet a)
	        throws BadLocationException {
	      if (str == null)
	        return;
	      String oldString = getText(0, getLength());
	      String newString = oldString.substring(0, offs) + str
	          + oldString.substring(offs);
	      try {
	        int test = Integer.parseInt(newString + "0") / 10;
	        if (test >= 0 && test <= Constants.MaxTolerance)
	        	super.insertString(offs, str, a);
	      } catch (NumberFormatException e) {
	      }
	    }
	  }

	}
