package gui;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

@SuppressWarnings("serial")
public class ImageDisplay extends JComponent
{
	private Image image;
	
	public ImageDisplay()
	{
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
	}
	
	public void setImage(Image image)
	{
		this.image = image;
		this.repaint();
	}
	
	public Image getImage()
	{
		return this.image;
	}
	
	protected void paintComponent(Graphics g)
	{
		this.getBorder().paintBorder(this, g, 0, 0, this.getWidth(), this.getHeight());
		
		Insets insets = this.getInsets();
		g.drawImage(this.image, insets.left, insets.top, this.getWidth() - insets.left - insets.right, this.getHeight() - insets.top - insets.bottom, this);
	}
}
