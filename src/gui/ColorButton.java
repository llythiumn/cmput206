package gui;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ColorButton extends JButton
{
	private Color color;
	
	public ColorButton()
	{
		color = Color.BLACK;
		
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
	}
	
	public void setColor(Color color)
	{
		this.color = color;
		this.repaint();
	}
	
	public Color getColor()
	{
		return this.color;
	}
	
	protected void paintComponent(Graphics g)
	{
		g.setColor(this.color);
		g.fill3DRect(0, 0, this.getWidth(), this.getHeight(), true);
	}
}
