import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import base.*;
import gui.*;
import ij.*;
import ij.gui.GUI;
import ij.process.*;
import ij.plugin.filter.*;

public class test_plugin implements PlugInFilter
{
	private final int flags = DOES_RGB | PARALLELIZE_IMAGES;
	
	private ImagePlus imp;
	
	/* (non-Javadoc)
	 * @see ij.plugin.filter.PlugInFilter#run(ij.process.ImageProcessor)
	 */
	@Override
	public void run(ImageProcessor ip)
	{
		//ip = convert(ip);
		//Objects
		FloatProcessor line, angle, poi;
		XYDerivativeEdgeDetection filter;
		PoiLocation poiFilter;
			//XYfilter setup
		filter = new XYDerivativeEdgeDetection(ip);
		filter.run();
			//aquire results
		line  = filter.getMagnitude();
		angle = filter.getAngles();
			//Poi Filter setup
		poiFilter = new PoiLocation(line, angle);
		poiFilter.run();
			//aquire results
		poi  = poiFilter.getPoi();
			//show results
		IJ.error("Show the results for " + ip.toString());
		ImagePlus Line, Angle, POI;
		Line  = new ImagePlus("Line",  line);
		Angle = new ImagePlus("Angle", angle);
		POI   = new ImagePlus("POI",   poi);
	}

	/* (non-Javadoc)
	 * @see ij.plugin.filter.PlugInFilter#setup(java.lang.String, ij.ImagePlus)
	 */
	@Override
	public int setup(String arg, ImagePlus imp)
	{
		this.imp = imp;
		
		return flags;
	}
	
	private FloatProcessor convert(ImageProcessor in) {
		int width = in.getWidth();
		int height = in.getHeight();
		FloatProcessor out = new FloatProcessor(width, height);
		float temp;
		for (int j = 0; j < height; j++)
			for (int i = 0; i < width; i++) {
				temp = (float)((((int)in.getPixelValue(i, j)) & 0x0000FF)) / 255f; 
				out.setf(i, j, temp*25);
			}
		return out;
	}
}

