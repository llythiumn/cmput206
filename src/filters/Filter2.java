package filters;
import filters.Filter.ptFunc;
import ij.process.*;

/**
 * @author James Helberg
 * 
 * Filter Interface will allow for easier time applying filters to images in ImageJ
 * 
 * Has the resulting image in an FloatProcessor type, and the passed filter should
 * be of a short 2D array type
 */
public abstract class Filter2 {

	/**
	 * Static values for indicating which extension to use (default is extend)
	 */
	public static final int F_EXTEND = Filter.F_EXTEND;
	public static final int F_TILE   = Filter.F_TILE;
	public static final int F_MIRROR = Filter.F_MIRROR;
	
	/**
	 * Filter is a 2D array that will be multiplied by the input images and added to the 
	 *   resulting image array list
	 * x_f, y_f are the size of the filter
	 * x_c, y_c are the center value points
	 * pt is a custom rounding class that rounds values based on specified formats
	 */
	private short[][] filter;
	private int       x_f, y_f;
	private int       x_c, y_c;
	private ptFunc    pt;
	
	/**
	 * Filter creation requires a 2D short array for filtering and centre positions for the
	 *   filter to be centred around
	 * @param short[][] passed - filter that will be applied to each point processed
	 * @param int centreX, centeeY - centre point of the filter
	 */
	public Filter2(short[][] passed, int centerX, int centerY){
		filter = passed;
		y_f    = filter.length;
		x_f    = filter[0].length;
		x_c    = centerX;
		y_c    = centerY;
		pt     = new f_extend();
	}
	/**
	 * Filter creation requires a 2D short array for filtering and centre positions for the
	 *   filter to be centred around
	 * @param short[][] passed - filter that will be applied to each point processed
	 * @param int centerX, centerY - centre point of the filter
	 * @param int TYPE - sets the type of rounding function we'll use
	 */
	public Filter2(short[][] passed, int centerX, int centerY, int TYPE){
		filter = passed;
		y_f    = filter.length;
		x_f    = filter[0].length;
		x_c    = centerX;
		y_c    = centerY;
		switch(TYPE) {
			case F_EXTEND:
				pt = new f_extend();
				break;
			case F_TILE:
				pt = new f_tile();
				break;
			case F_MIRROR:
				pt = new f_mirror();
				break;
			default:
				pt = new f_extend();
				break;
		}
	}
	
	/**
	 * Run is the process automated to use the abstract
	 * @param ImageProcessor One,Two - images to be processed
	 * @return FloatProcessor - returned image for display
	 */
	public FloatProcessor run(ImageProcessor One, ImageProcessor Two) {
		int width  = Math.min(One.getWidth(),Two.getWidth());
		int height = Math.min(One.getHeight(),Two.getHeight());
		FloatProcessor Out    = new FloatProcessor(width, height);
		//abstract setup
		this.setUp();
		//loop through all points
		int pt_y;
		int pt_x;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				this.newPt();
				for (int fy = 0; fy < y_f; fy++) {
					pt_y = pt.round(y + (fy - y_c), height);
					for (int fx = 0; fx < x_f; fx++) {
						pt_x = pt.round(x + (fx - x_c), width);
						this.pt((short)One.get(pt_x,pt_y),(short)Two.get(pt_x,pt_y),filter[fy][fx]);
						this.local(pt_x, pt_y);
					}
				}
				Out.setf(x,y,this.get());
			}
		}
		return Out;
	}
	
	/**
	 * Abstract methods for implementation by child methods
	 */
	protected abstract void  setUp();
	protected abstract void  newPt();
	protected abstract void  pt(float One, float Two, float F);
	protected          void  local(int x, int y) {};
	protected abstract float get();

/**
 * Abstract method for use in boundary rounding
 * @param pt - the point being passed to be rounded
 * @param pt_max - the upper boundary of the point
 */
interface ptFunc {
	public int round(int pt, int pt_max);
}

/**
 * f_extend uses the point closest to the passed point
 */
class f_extend implements ptFunc {
	public int round(int pt, int pt_max) {
		if (pt < 0) {
			return 0;
		} else if (pt >= pt_max) {
			return pt_max - 1;
		} else {
			return pt;
		}
	}
}

/**
 * f_tile will tile the image when applying the filter
 */
class f_tile implements ptFunc {
	public int round(int pt, int pt_max) {
		if (pt < 0) {
			return (pt_max + pt)%pt_max;
		} else if (pt >= pt_max) {
			return pt%pt_max;
		} else {
			return pt;
		}
	}
}

/**
 * f_mirror uses the point mirrored to the interior portion of the 
 */
class f_mirror implements ptFunc {
	public int round(int pt, int pt_max) {
		if (pt < 0) {
			return - (pt%pt_max);
		} else if (pt >= pt_max) {
			return pt_max - (pt%pt_max) - 1;
		} else {
			return pt;
		}
	}
}
}