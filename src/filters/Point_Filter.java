package filters;
import ij.process.*;

/**
 * @author James Helberg
 * 
 * Point Filter Interface will allow for easier time applying filters to images in ImageJ
 * 
 * Has the resulting image in an floatProcessor type
 */
public abstract class Point_Filter {
	
	/**
	 * Point filter applies a single function to every point within an
	 * image
	 * 
	 * @param 
	 */
	public Point_Filter() {
	}
	
	/**
	 * Run is the process automated to use the abstract
	 * @param ImageProcessor In - image to be processed
	 * @return FloatProcessor - returned image for display
	 */
	public FloatProcessor run(ImageProcessor In) {
		int width = In.getWidth();
		int height = In.getHeight();
		//abstract setup
		this.setUp();
		FloatProcessor result = new FloatProcessor(width, height);
		//loop through all points
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				result.setf(x,y,this.pt(In.get(x,y)));
			}
		}
		return result;
	}
	
	/**
	 * Abstract methods for implementation by child methods
	 */
	private void setUp() {};
	protected abstract float pt(float I);
	
}
