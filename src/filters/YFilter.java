package filters;

public class YFilter extends Filter {
	
	private float Sum;
	private static short[][] filter = {{-5},{0},{5}};
	
	public YFilter() {
		super(filter,0,1, Filter.F_EXTEND);
	}

	@Override
	protected void setUp() {
		//Nothin
	}
	@Override
	protected void newPt() {
		Sum = 0;
	}

	@Override
	protected void pt(float I, float F) {
		// TODO Auto-generated method stub
		Sum = Sum + I * F;
	}

	@Override
	protected float get() {
		// TODO Auto-generated method stub
		return Sum;
	}
	
}
