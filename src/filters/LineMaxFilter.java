package filters;

import ij.IJ;

import java.util.ArrayList;

import base.Point;

/**
 * @author James
 * 
 * Filter will use angles and a line image to calculate Points-of-Interest
 */
public class LineMaxFilter extends Filter2 {

	public  static final short LEAVE = 0;
	public  static final short CHECK = 10;
	public  static final short CENTRE= 20;
	private static final short[][] CROSS_FILTER =
		{{LEAVE,CHECK,LEAVE},{CHECK,CENTRE,CHECK},{LEAVE,CHECK,LEAVE}};
	
	private ArrayList<Float> list; 
	private ArrayList<Point<Integer>> pts;
	//private ArrayList<Point<Float>>   result;
	private ArrayList<Point<Integer>>   result;
	private float centre, summation;
	private boolean usePt, useFilt;
	private float threshold;
	
	public LineMaxFilter(float threshold) {
		super(CROSS_FILTER,1,1,Filter.F_EXTEND);
		this.threshold = threshold;
	}
	public LineMaxFilter(short[][] passed, int centerX, int centerY, int TYPE, float threshold) {
		super(passed, centerX, centerY, TYPE);
		this.threshold = threshold;
	}
	public LineMaxFilter(short[][] passed, int centerX, int centerY, float threshold) {
		super(passed, centerX, centerY);
		this.threshold = threshold;
	}
	
	
	@Override
	protected void setUp() {
		list    = new ArrayList<Float>();
		pts     = new ArrayList<Point<Integer>>();
		//result  = new ArrayList<Point<Float>>();
		result  = new ArrayList<Point<Integer>>();
		centre  = 0;
		usePt   = false;
		useFilt = false;
	}

	@Override
	protected void newPt() {
		//list.clear();
		pts.clear();
		useFilt = false;
		summation = 0.0f;
	}
	
	@Override
	protected void pt(float One, float Two, float F) {
		if (Two > threshold) {
			//usePt = true;
			if (F > 15f) {
				centre = Two;
				usePt = true;
			}
			if (F > 5) {
				list.add(Two);
				summation = summation + Two;
			}
		} else {
			usePt = false;
		}
	}

	@Override
	protected void local(int x, int y) {
		if (usePt) {
			//pts.add(new Point<Integer>(x,y));
			result.add(new Point<Integer>(x,y));
			useFilt = true;
		}
	}

	@Override
	protected float get() {
		useFilt = false;
		for (Float pt : list) {
			if (pt >= centre) {
				return 0;
			}
		}
		addPoint();
		return centre;
	}
	
	private void addPoint() {
		/*//Commented for temporary Integer setup
		float x = 0f;
		float y = 0f;
		for(Point<Integer> pt : pts) {
			x = x + pt.getX();
			y = y + pt.getY();
		}
		result.add(new Point<Float>(x,y));*/
	}

	//public ArrayList<Point<Float>> getPoints() {
	public ArrayList<Point<Integer>> getPoints() {
		return result;
	}
	
}
