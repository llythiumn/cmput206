package filters;

public class AngleFilter extends Point_Filter2 {
	private double angle_add;
	/**
	 * Performs an ArcTan function on an XFiltered and a YFiltered image
	 * returns results in radians rotated to be clockwise parallel to shape's edge
	 * Pass the parameters in terms of One = X and Two = Y
	 * @param angle_rotate angle in radians that will rotate the image by.
	 */
	public AngleFilter() {
		super();
		angle_add = 0;
	}
	public AngleFilter(double angle_rotate) {
		super();
		angle_add = angle_rotate % (2 * Math.PI);
	}
	
	@Override
	protected float pt(float One, float Two) {
		// calculate the angle (using acos() to find pi/2 and pi in case different machines
		// use radians or degrees differently).
		double angle = Math.atan2(Two, One) + angle_add;
		if (angle > (2*Math.PI)) {
			angle = angle - 2 * Math.PI;
		} else 
		if (angle < 0) {
			angle = angle + 2 * Math.PI;
		}
		return (float) angle;
	}

}
