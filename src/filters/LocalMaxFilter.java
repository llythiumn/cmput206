package filters;

import java.util.ArrayList;

public class LocalMaxFilter extends Filter {

	public  static final int LEAVE = 0x00;
	public  static final int CHECK = 0x01;
	public  static final int CENTRE= 0x02;
	private static final short[][] CROSS_FILTER =
		{{LEAVE,CHECK,LEAVE},{CHECK,CENTRE,CHECK},{LEAVE,CHECK,LEAVE}};
	
	private ArrayList<Float> list; 
	private float centre;
	
	public LocalMaxFilter() {
		super(CROSS_FILTER,1,1,Filter.F_EXTEND);
	}
	public LocalMaxFilter(short[][] passed, int centerX, int centerY, int TYPE) {
		super(passed, centerX, centerY, TYPE);
		// TODO Auto-generated constructor stub
	}
	public LocalMaxFilter(short[][] passed, int centerX, int centerY) {
		super(passed, centerX, centerY);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	protected void setUp() {
		list   = new ArrayList<Float>();
		centre = 0;
	}

	@Override
	protected void newPt() {
		// TODO Auto-generated method stub
		list.clear();
	}

	@Override
	protected void pt(float I, float F) {
		switch((int)F) {
		case CHECK:
			list.add(I);
			break;
		case CENTRE:
			centre = I;
			break;
		default:
			break;
		}
	}

	@Override
	protected float get() {
		// TODO Auto-generated method stub
		for (Float pt : list) {
			if (pt > centre) {
				return 0;
			}
		}
		return centre;
	}

}
