package filters;
import ij.process.*;

/**
 * @author James Helberg
 * 
 * Point Filter Interface will allow for easier time applying filters to images in ImageJ
 * 
 * Has the resulting image in an floatProcessor type
 */
public abstract class Point_Filter2 {
	
	/**
	 * Point filter applies a single function to every point within a pair of images
	 * 
	 * @param 
	 */
	public Point_Filter2() {
	}
	
	/**
	 * Run is the process automated to use the abstract
	 * @param ImageProcessor One, Two - images to be processed
	 * @return FloatProcessor - returned image for display
	 */
	public FloatProcessor run(ImageProcessor One, ImageProcessor Two) {
		int width = Math.min(One.getWidth(),Two.getWidth());
		int height = Math.min(One.getHeight(),Two.getHeight());
		//abstract setup
		this.setUp();
		FloatProcessor result = new FloatProcessor(width, height);
		//loop through all points
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				result.setf(x,y,this.pt(One.get(x,y),Two.get(x,y)));
			}
		}
		return result;
	}
	
	/**
	 * Abstract methods for implementation by child methods
	 */
	private void setUp() {};
	protected abstract float pt(float One, float Two);
}
