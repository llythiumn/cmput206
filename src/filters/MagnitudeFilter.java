package filters;

public class MagnitudeFilter extends Point_Filter2 {

	public MagnitudeFilter() {
		super();
	}
	
	@Override
	protected float pt(float One, float Two) {
		// return the sqrt ( One^2 + Two^2 )
		// Pathagorean
		return (float) Math.sqrt(Math.pow(One, 2) + Math.pow(Two, 2));
	}

}
