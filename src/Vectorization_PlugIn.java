import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import base.*;
import gui.*;
import ij.*;
import ij.gui.GUI;
import ij.process.*;
import ij.plugin.filter.*;

public class Vectorization_PlugIn implements ExtendedPlugInFilter
{
	private final int flags = DOES_RGB | PARALLELIZE_IMAGES;
	
	private ImagePlus imp;
	private ApproximateColor[] colors;
	
	/* (non-Javadoc)
	 * @see ij.plugin.filter.PlugInFilter#run(ij.process.ImageProcessor)
	 */
	@Override
	public void run(ImageProcessor ip)
	{
		//Objects
		FloatProcessor bw, line, angle;
		//TODO: convert back to floats at later point
		//ArrayList<Point<Float>> list;
		ArrayList<Point<Integer>> list;
		ArrayList<GeneralPath> Paths;
		//SAMPLES
		ImagePlus One, Two;
		//Filters
		ColorExtraction colorExtract;
		XYDerivativeEdgeDetection filter;
		PoiLocation poiFilter;
		BinarySvgCreation BSVG;
		//Constant
		SvgMerge Image = new SvgMerge();
		Image.setup();
		
		int count = colors.length;
		for(int i = 0; i < count; i++) {
			//ColorExtraction
			colorExtract = new ColorExtraction(ip,colors[i]);
			colorExtract.run();
			bw = colorExtract.getBW();
			//XYfilter setup
			filter = new XYDerivativeEdgeDetection(bw);
			filter.run();
			//aquire results
			line  = filter.getMagnitude();
			angle = filter.getAngles();
			//Poi Filter setup
			poiFilter = new PoiLocation(line, angle);
			poiFilter.run();
			//aquire results
			list = poiFilter.getPoints();
			
			BSVG = new BinarySvgCreation(line, angle, colors[i], list);
			BSVG.run();
			Paths = BSVG.getPoints();
			
			Image.addPaths(Paths, colors[i]);
		}
		
		Image.write_SVG();
		
		
	}

	/* (non-Javadoc)
	 * @see ij.plugin.filter.PlugInFilter#setup(java.lang.String, ij.ImagePlus)
	 */
	@Override
	public int setup(String arg, ImagePlus imp)
	{
		this.imp = imp;
		
		return flags;
	}

	/* (non-Javadoc)
	 * @see ij.plugin.filter.ExtendedPlugInFilter#setNPasses(int)
	 */
	@Override
	public void setNPasses(int nPasses)
	{
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see ij.plugin.filter.ExtendedPlugInFilter#showDialog(ij.ImagePlus, java.lang.String, ij.plugin.filter.PlugInFilterRunner)
	 */
	@Override
	public int showDialog(ImagePlus imp, String command, PlugInFilterRunner pfr)
	{
		// Show the dialog. Ignore the pfr parameter.
		Object notifiedOnClose = new Object();
		ColorSelectionFrame frame = new ColorSelectionFrame(imp, notifiedOnClose);
		frame.pack();
		GUI.center(frame);
		frame.setVisible(true);
		
		synchronized (notifiedOnClose)
		{
			while (frame.isVisible())
			{
				try
				{
					notifiedOnClose.wait();
				}
				catch (InterruptedException e)
				{}
			}
		}
		
		this.colors = frame.getColors();
		
		if (frame.wasCancelled())
			return ExtendedPlugInFilter.DONE;
		else
			return flags;
	}
}

